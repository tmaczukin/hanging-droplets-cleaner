package cleaner

import (
	"io/ioutil"
	"regexp"
)

type MachinesFinderInterface interface {
	ListMachines(*regexp.Regexp) ([]string, error)
}

type MachinesFinder struct {
	machinesDirectory string
}

func (m *MachinesFinder) ListMachines(runnerPrefixRegexp *regexp.Regexp) (machines []string, err error) {
	entries, err := ioutil.ReadDir(m.machinesDirectory)
	if err != nil {
		return
	}

	for _, entry := range entries {
		name := entry.Name()
		if !entry.IsDir() || !runnerPrefixRegexp.MatchString(name) {
			continue
		}
		machines = append(machines, name)
	}

	return
}

func NewMachinesFinder(machinesDirectory string) *MachinesFinder {
	return &MachinesFinder{
		machinesDirectory: machinesDirectory,
	}
}
